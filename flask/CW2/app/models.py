from app import db , app
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user


#  Object with 5 variables to add to database
class savetasks(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    task = db.Column(db.String(100))    #   task name
    desc = db.Column(db.String(1000))   #   task description
    time = db.Column(db.DateTime)       #records when task was created
    status = db.Column(db.Boolean)  #  is task completed or uncompleted

itemtable = db.Table('itemtable', db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('item_id', db.Integer, db.ForeignKey('item.id'))
)

receipttable = db.Table('receipttable', db.Model.metadata,
    db.Column('receipt_id', db.Integer, db.ForeignKey('receipt.id')),
    db.Column('transcriptitem_id', db.Integer, db.ForeignKey('transcriptitem.id'))
)

class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30),unique=True)
    password = db.Column(db.String(30))
    useritems = db.relationship('Item', secondary=itemtable)
    transcript = db.relationship('Receipt',backref='trans')

class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    itemname = db.Column(db.String(100))
    itemtype = db.Column(db.String(30))
    itemprice = db.Column(db.Float)
    itemusers = db.relationship('User', secondary=itemtable,backref='itemsthisuser')
    Quantity = db.Column(db.Integer)

class Receipt(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    orders = db.relationship('Transcriptitem', secondary=receipttable)
    userreceipts = db.Column(db.Integer, db.ForeignKey('user.id'))
    time = db.Column(db.DateTime)

class Transcriptitem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    itemname = db.Column(db.String(100))
    itemtype = db.Column(db.String(30))
    itemprice = db.Column(db.Float)
    itemreceipt = db.relationship('Receipt', secondary=receipttable,backref='receipt')
    Quantity = db.Column(db.Integer)

