from flask_wtf import Form
from wtforms import StringField, SubmitField , TextAreaField
from wtforms.validators import DataRequired


#  3 fields that can be changed by the user, validators make sure they aren't blank
class createtask(Form):
    Task = StringField('Enter Task',validators=[DataRequired()])
    Area = StringField('Area',validators=[DataRequired()])
    Desc = TextAreaField('Enter Description',validators=[DataRequired()])



