from app import db , app
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user


#  Object with 5 variables to add to database
class savetasks(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    task = db.Column(db.String(100))    #   task name
    desc = db.Column(db.String(1000))   #   task description
    time = db.Column(db.DateTime)       #records when task was created
    status = db.Column(db.Boolean)  #  is task completed or uncompleted

itemtable = db.Table('itemtable', db.Model.metadata,
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('item_id', db.Integer, db.ForeignKey('item.id'))
)

class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30),unique=True)
    password = db.Column(db.String(30),unique=True)
    useritems = db.relationship('Item', secondary=itemtable,backref='users',lazy='dynamic')

class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    itemname = db.Column(db.String(100),unique=True)
    itemtype = db.Column(db.String(30),unique=True)
    itemprice = db.Column(db.Integer)
    itemusers = db.relationship('User', secondary=itemtable,backref='itemsthisuser',lazy='dynamic')
    time = db.Column(db.DateTime)
    
