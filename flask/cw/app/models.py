from app import db , app
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user


#  Object with 5 variables to add to database
class savetasks(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    task = db.Column(db.String(100))    #   task name
    desc = db.Column(db.String(1000))   #   task description
    time = db.Column(db.DateTime)       #records when task was created
    status = db.Column(db.Boolean)  #  is task completed or uncompleted


class User(UserMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30),unique=True)
    u = db.Column(db.String(30),unique=True)