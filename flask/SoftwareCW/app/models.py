from app import db

#  Object with 5 variables to add to database
class savetasks(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    task = db.Column(db.String(100))    #   task name
    desc = db.Column(db.String(1000))   #   task description
    area = db.Column(db.String(100))
    time = db.Column(db.DateTime)       #records when task was created
    status = db.Column(db.Boolean)  #  is task completed or uncompleted