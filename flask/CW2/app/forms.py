from flask_wtf import Form
from wtforms import StringField, SubmitField , TextAreaField , PasswordField , IntegerField , FloatField
from wtforms.validators import DataRequired


#  2 fields that can be changed by the user, validators make sure they aren't blank
class userdetails(Form):
    username = StringField('Enter Task',validators=[DataRequired()])
    password = PasswordField('Enter Password',validators=[DataRequired()])

class itemdetails(Form):
    itemname = StringField('itemname',validators=[DataRequired()])
    itemtype = StringField('itemtype',validators=[DataRequired()])
    itemprice = FloatField('itemprice',validators=[DataRequired()])

class changep(Form):
    oldpassword = PasswordField('oldpassword',validators=[DataRequired()])
    newpassword = PasswordField('newpassword',validators=[DataRequired()])
    newpassword2 = PasswordField('newpassword2',validators=[DataRequired()])



