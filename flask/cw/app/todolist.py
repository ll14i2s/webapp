from flask import  render_template , request , redirect
from app import app , db  , models
from .forms import createtask
import datetime
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.user_loader
def load_user(user_id):
    return models.User.query.get(int(user_id))

#  Homepage with initial create task button that redirects to create task page
@app.route('/')
def index():
    return render_template('Homepage.html',
                            title = 'Home')


#  Create task page with Task and task description that will add task to data base
@app.route("/Create", methods=["POST","GET"])
def create():
    form = createtask()
    us = models.User.query.all()
    for x in us:
        if form.Desc.data == x.username:
            login_user(x)
            return 'LOGGED IN'
    if form.validate_on_submit():  #  Check task and description fields arent blank
        newuser = models.User(username=form.Task.data)
        db.session.add(newuser)
        db.session.commit()                          #  Add task to database
        
    return render_template('index.html',
                            form=form,
                            title = 'Create a task')

#  Will simply view all completed tasks that are in database  with status = True 
@app.route('/Completed-Tasks', methods=["POST","GET"])
@login_required
def completed():
    logout_user()
    return render_template('completedtasks.html',
                             uctasks = models.savetasks.query.all(),
                             title = 'Completed Tasks')

# View uncompleted tasks with status = False and can mark tasks as complete
@app.route('/Uncompleted-Tasks', methods=["POST","GET"])
@login_required
def uncompleted():
    return render_template('uctasks.html',
                            uctasks = models.savetasks.query.all(),
                            title = 'Uncompleted Tasks',)

#  Mark task as complete by changing status and moving to complete task page
@app.route('/mark/<int:id>')
def mark(id):
    form = createtask()
    marktask=models.savetasks.query.filter_by(id=id).first()
    if (marktask.status == False):  # to only changes false value
        marktask.status = not marktask.status
    db.session.commit()  # update database
    return render_template('uctasks.html',
                            title = 'Uncompleted Tasks',
                            uctasks = models.savetasks.query.all())