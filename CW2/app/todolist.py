from flask import  render_template , request , redirect
from .forms import userdetails, itemdetails , changepass
from flask_admin.contrib.sqla import ModelView
from app import app , db  , models , admin
from .models import User,Item
import datetime
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

login_manager = LoginManager()
login_manager.init_app(app)

admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Item, db.session))

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

#  Homepage with initial create task button that redirects to create task page
@app.route('/',methods=["POST","GET"])
def index():
    form = userdetails()
    users = models.User.query.all()
    if form.validate_on_submit():
        for x in users:
            if form.username.data == 'Admin' and form.password.data == 'Admin':
                login_user(x)
                return render_template('Adminpage.html')
            if form.username.data == x.username and form.password.data == x.password:
                login_user(x)
                return render_template('loggedinhome.html',
                             allitems = models.Item.query.all(),
                             title = 'Shop')
    return render_template('Homepage.html',
                            form=form,
                            title = 'Home')

@app.route('/Stock',methods=["POST","GET"])
def Stock():
    form = itemdetails()
    if form.validate_on_submit():
        newitem = models.Item(itemname=form.itemname.data,itemtype=form.itemtype.data,itemprice=form.itemprice.data)
        db.session.add(newitem)
        db.session.commit()
    
    return render_template('Stock.html',
                            form=form,
                            allitems = models.Item.query.all(),
                            title = 'Add or Remove Items')
#  Create task page with Task and task description that will add task to data base
@app.route("/Create", methods=["POST","GET"])
def create():
    form = userdetails()
    if form.validate_on_submit():  #  Check task and description fields arent blank
        newuser = models.User(username=form.username.data,password=form.password.data)
        db.session.add(newuser)
        db.session.commit()                          #  Add task to database
        
    return render_template('index.html',
                            form=form,
                            title = 'Create an account')

#  Will simply view all completed tasks that are in database  with status = True 
@app.route('/Completed-Tasks', methods=["POST","GET"])
@login_required
def completed():
    if current_user.is_authenticated:
        return render_template('loggedinhome.html',
                                allitems = models.Item.query.all(),
                                title = 'Shop')
    return render_template('completedtasks.html',
                             allitems = models.Item.query.all(),
                             title = 'Shop')

@app.route('/Account', methods=["POST","GET"])
@login_required
def account():
    form = changepass()
    if form.validate_on_submit():  #  Check task and description fields arent blank
        if form.oldpassword.data == current_user.password:
            return 'YAY'
    return render_template('account.html',
                             form=form,
                             title = 'Shop')

@app.route('/logout', methods=["POST","GET"])
def logout():
    logout_user()
    return render_template('completedtasks.html',
                             allitems = models.Item.query.all(),
                             title = 'Shop')


@app.route('/addtobasket/<int:id>')
@login_required
def addtobasket(id):
    additemtob = models.Item.query.filter_by(id=id).first()
    additemtob.users.append(current_user)
    current_user.itemsthisuser.append(additemtob)
    db.session.commit()
    return render_template('loggedinhome.html',
                             allitems = models.Item.query.all(),
                             title = 'Completed Tasks')

@app.route('/Basket')
def basket():
    if current_user.is_authenticated:
        totalprice = 0.0
        for uss in current_user.itemsthisuser:
            totalprice = totalprice + uss.itemprice
        return render_template('basket.html',
                                usernow = current_user,
                                title = 'Basket',
                                total=totalprice)
    return redirect('/')


# View uncompleted tasks with status = False and can mark tasks as complete
@app.route('/Uncompleted-Tasks', methods=["POST","GET"])
@login_required
def uncompleted():
    return render_template('uctasks.html',
                            uctasks = models.savetasks.query.all(),
                            title = 'Uncompleted Tasks',)

#  Mark task as complete by changing status and moving to complete task page
@app.route('/mark/<int:id>')
def mark(id):
    form = userdetails()
    marktask=models.savetasks.query.filter_by(id=id).first()
    if (marktask.status == False):  # to only changes false value
        marktask.status = not marktask.status
    db.session.commit()  # update database
    return render_template('uctasks.html',
                            title = 'Uncompleted Tasks',
                            uctasks = models.savetasks.query.all())