from app import app, db, admin
from flask_admin.contrib.sqla import ModelView

from app.models import Property, Landlord

admin.add_view(ModelView(Property, db.session))
admin.add_view(ModelView(Landlord, db.session))
admin.add_view(ModelView(Student, db.session))

@app.route('/')
def index():
    return "Hello world"
