from flask import  render_template , request , redirect , flash
from app import app , db  , models
from .forms import createtask
import datetime

#  Homepage with initial create task button that redirects to create task page
@app.route('/')
def index():
    return render_template('Homepage.html',
                            title = 'Home')


#  Create task page with Task and task description that will add task to data base
@app.route("/Create", methods=["POST","GET"])
def create():
    form = createtask()
    if form.validate_on_submit():  #  Check task and description fields arent blank
        newtask = models.savetasks(task=form.Task.data,
                                desc=form.Desc.data,
                                time=datetime.datetime.utcnow(),
                                status = False)
        db.session.add(newtask)
        db.session.commit()                          #  Add task to database
        
    return render_template('index.html',
                            form=form,
                            title = 'Create a task')

@app.route("/add", methods=["POST","GET"])
def add():
    form = createtask()
    if form.validate_on_submit():  #  Check task and description fields arent blank
        newtask = models.savetasks(task=form.Task.data,
                                desc=form.Desc.data,
                                area=form.Area.data,
                                time=datetime.datetime.utcnow(),
                                status = False)
        db.session.add(newtask)
        db.session.commit()                          #  Add task to database
        
    return render_template('add.html',
                            form=form,
                            title = 'Create a task')

#  Will simply view all completed tasks that are in database  with status = True 
@app.route('/Completed-Tasks', methods=["POST","GET"])
def completed():
    form = createtask()
    if form.validate_on_submit():  #  Check task and description fields arent blank
        newLocation = models.savetasks(task=form.Task.data,
                                desc=form.Desc.data,
                                time=datetime.datetime.utcnow(),
                                area=form.Area.data,
                                status = False)
        db.session.add(newLocation)
        db.session.commit()  
    return render_template('completedtasks.html',
                             uctasks = models.savetasks.query.all(),
                             title = 'Add Location',
                             form=form)

# View uncompleted tasks with status = False and can mark tasks as complete
@app.route('/Uncompleted-Tasks', methods=["POST","GET"])
def uncompleted():
    return render_template('uctasks.html',
                            uctasks = models.savetasks.query.all(),
                            title = 'Locations')

#  Mark task as complete by changing status and moving to complete task page
@app.route('/delete/<int:id>')
def mark(id):
    location=models.savetasks.query.filter_by(id=id).first()
    db.session.delete(location)
    db.session.commit()  # update database
    return render_template('uctasks.html',
                            title = 'Report Symptoms',
                            uctasks = models.savetasks.query.all())